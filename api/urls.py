from django.conf.urls import url

from api.views import TotalOrderValue, ReturnCountTotalCost

urlpatterns = [
    url(r'^returns/value/returns',TotalOrderValue.as_view(table_name='aggregated_order_line', field_name='return_date')),
    url(r'^returns/value/orders',TotalOrderValue.as_view(table_name='aggregated_order_line', field_name='order_created_date')),
    url(r'^returns/cost/returns', ReturnCountTotalCost.as_view()),
]
