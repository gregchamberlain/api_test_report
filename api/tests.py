from django.test import Client
from sqlalchemy import text
from api.helpers import sqlengine
import unittest
from datetime import date

engine = sqlengine()
client = Client()

RETURN_VALUE_QUERY = """
SELECT
  COALESCE(SUM(product_price))
FROM
  aggregated_order_line
WHERE
  return_date BETWEEN :start_date AND :end_date
"""

ORDER_VALUE_QUERY = """
SELECT
  COALESCE(SUM(product_price))
FROM
  aggregated_order_line
WHERE
  order_created_date BETWEEN :start_date AND :end_date
"""

RETURN_COST_QUERY = """
SELECT
  COALESCE(SUM(reverse_shipping_cost + restocking_cost))
FROM
  aggregated_order_line
WHERE
  order_created_date BETWEEN :start_date AND :end_date
"""

CATEGORY_FILTER = " AND product_category = :category"
GENDER_FILTER = " AND user_gender = :gender"

# /returns/value/orders
class TotalOrderValueOrdersTests(unittest.TestCase):

  def setUp(self):
    self.all_resp = client.get('/returns/value/orders?start_date=2016-01-01&end_date=2016-07-01')
    self.all_value = self.all_resp.data["data"][0]["value"]

  def test_400_response_with_bad_request(self):
    no_start_or_end_response = client.get('/returns/value/orders')
    no_start_response = client.get('/returns/value/orders?end_date=2016-01-01')
    no_end_response = client.get('/returns/value/orders?start_date=2016-01-01')
    self.assertEqual(no_start_or_end_response.status_code, 400)
    self.assertEqual(no_start_response.status_code, 400)
    self.assertEqual(no_end_response.status_code, 400)

  def test_200_response_on_good_request(self):
    self.assertEqual(self.all_resp.status_code, 200)

  def test_filters_by_category(self):
    db_data = engine.execute(text(ORDER_VALUE_QUERY + CATEGORY_FILTER),
    start_date=date(2016,1,1),
    end_date=date(2016,7,1),
    category="coat").fetchall()
    coat_response = client.get('/returns/value/orders?start_date=2016-01-01&end_date=2016-07-01&category=coat')
    coat_value = coat_response.data["data"][0]["value"]
    self.assertEqual(coat_value, db_data[0][0])

  def test_always_return_number(self):
    none_response = client.get('/returns/value/orders?start_date=2016-01-01&end_date=2016-07-01&category=categorythatdoesnotexist')
    none_value = none_response.data["data"][0]["value"]
    self.assertIn(type(self.all_value), [int, float])
    self.assertIn(type(none_value), [int, float])

  def test_filters_by_gender(self):
    m_resp = client.get('/returns/value/orders?start_date=2016-01-01&end_date=2016-07-01&user_gender=m')
    m_value = m_resp.data["data"][0]["value"]
    db_data = engine.execute(text(ORDER_VALUE_QUERY + GENDER_FILTER),
    start_date=date(2016,1,1),
    end_date=date(2016,7,1),
    gender="m").fetchall()
    self.assertEqual(m_value, db_data[0][0])

# /returns/value/returns
class TotalOrderValueReturnsTests(unittest.TestCase):

  def setUp(self):
    self.all_resp = client.get('/returns/value/returns?start_date=2016-01-01&end_date=2016-07-01')
    self.all_value = self.all_resp.data["data"][0]["value"]

  def test_400_response_with_bad_request(self):
    no_start_or_end_response = client.get('/returns/value/returns')
    no_start_response = client.get('/returns/value/returns?end_date=2017-01-01')
    no_end_response = client.get('/returns/value/returns?start_date=2016-01-01')
    self.assertEqual(no_start_or_end_response.status_code, 400)
    self.assertEqual(no_start_response.status_code, 400)
    self.assertEqual(no_end_response.status_code, 400)

  def test_200_response_on_good_request(self):
    self.assertEqual(self.all_resp.status_code, 200)

  def test_filters_by_category(self):
    db_data = engine.execute(text(RETURN_VALUE_QUERY + CATEGORY_FILTER),
    start_date=date(2016,1,1),
    end_date=date(2016,7,1),
    category="coat").fetchall()
    coat_response = client.get('/returns/value/returns?start_date=2016-01-01&end_date=2016-07-01&category=coat')
    coat_value = coat_response.data["data"][0]["value"]
    self.assertEqual(coat_value, db_data[0][0])

  def test_always_return_number(self):
    none_response = client.get('/returns/value/returns?start_date=2016-01-01&end_date=2016-07-01&category=categorythatdoesnotexist')
    none_value = none_response.data["data"][0]["value"]
    self.assertIn(type(self.all_value), [int, float])
    self.assertIn(type(none_value), [int, float])

  def test_filters_by_gender(self):
    m_resp = client.get('/returns/value/returns?start_date=2016-01-01&end_date=2016-07-01&user_gender=m')
    m_value = m_resp.data["data"][0]["value"]
    db_data = engine.execute(text(RETURN_VALUE_QUERY + GENDER_FILTER),
    start_date=date(2016,1,1),
    end_date=date(2016,7,1),
    gender="m").fetchall()
    self.assertEqual(m_value, db_data[0][0])

# /returns/cost/returns
class ReturnCountTotalCostTests(unittest.TestCase):

  def setUp(self):
    self.all_resp = client.get('/returns/cost/returns?start_date=2016-01-01&end_date=2016-07-01')
    self.all_cost = self.all_resp.data["data"][0]["cost"]

  def test_400_response_with_bad_request(self):
    no_start_or_end_response = client.get('/returns/cost/returns')
    no_start_response = client.get('/returns/cost/returns?end_date=2016-01-01')
    no_end_response = client.get('/returns/cost/returns?start_date=2016-01-01')
    self.assertEqual(no_start_or_end_response.status_code, 400)
    self.assertEqual(no_start_response.status_code, 400)
    self.assertEqual(no_end_response.status_code, 400)

  def test_200_response_on_good_request(self):
    self.assertEqual(self.all_resp.status_code, 200)

  def test_filters_by_category(self):
    db_data = engine.execute(text(RETURN_COST_QUERY + CATEGORY_FILTER),
    start_date=date(2016,1,1),
    end_date=date(2016,7,1),
    category="coat").fetchall()
    coat_response = client.get('/returns/cost/returns?start_date=2016-01-01&end_date=2016-07-01&category=coat')
    coat_cost = coat_response.data["data"][0]["cost"]
    self.assertEqual(coat_cost, db_data[0][0])

  def test_always_return_number(self):
    none_response = client.get('/returns/cost/returns?start_date=2016-01-01&end_date=2016-07-01&category=categorythatdoesnotexist')
    none_cost = none_response.data["data"][0]["cost"]
    self.assertIn(type(self.all_cost), [int, float])
    self.assertIn(type(none_cost), [int, float])

  def test_filters_by_gender(self):
    m_resp = client.get('/returns/cost/returns?start_date=2016-01-01&end_date=2016-07-01&user_gender=m')
    m_cost = m_resp.data["data"][0]["cost"]
    db_data = engine.execute(text(RETURN_COST_QUERY + GENDER_FILTER),
    start_date=date(2016,1,1),
    end_date=date(2016,7,1),
    gender="m").fetchall()
    self.assertEqual(m_cost, db_data[0][0])
