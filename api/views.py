from datetime import datetime, timedelta
from django.shortcuts import render

# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView
from sqlalchemy import text

from api.helpers import listtodict, addrootelement, sqlengine
from api.serializers import  CostOutputSerializer, DistinctInputSerializer, ValueOutputSerializer

class TotalOrderValue(APIView):
    table_name = None
    field_name = None
    def get(self, request):
        engine = sqlengine()
        input = DistinctInputSerializer(data=self.request.query_params)
        input.is_valid(raise_exception=True)
        data = input.validated_data
        base = 'select coalesce(sum(ans.price),0) from (select sum(product_price) AS price from aggregated_order_line  where '+ self.field_name+' between :start_date and :end_date '
        category_filter = ('' if 'category' not in data else ' and product_category=:category ')
        gender_filter = (" and user_gender = :gender " if 'user_gender' in data else '')
        group_filter = (" group by order_id) ")
        db_data = engine.execute(text(base + category_filter + gender_filter + group_filter + 'ans'),
                                 start_date=data['start_date'],
                                 end_date=data['end_date'],
                                 gender=(data['user_gender'] if 'user_gender' in data else None),
                                 category=(data['category'] if 'category' in data else None)).fetchall()
        output = ValueOutputSerializer(listtodict(db_data, ['value']), many=True)
        return Response(addrootelement(output.data))

class ReturnCountTotalCost(APIView):
    def get(self, request):
        engine = sqlengine()
        input = DistinctInputSerializer(data=self.request.query_params)
        input.is_valid(raise_exception=True)
        data = input.validated_data
        base = 'select sum(ans.cost) from (select sum(reverse_shipping_cost + restocking_cost) AS cost from aggregated_order_line  where order_created_date between :start_date and :end_date '
        category_filter = ('' if 'category' not in data else ' and product_category=:category ')
        gender_filter = (" and user_gender = :gender " if 'user_gender' in data else '')
        group_filter = (" group by order_id) ")
        db_data = engine.execute(text(base + category_filter + gender_filter + group_filter + 'ans' ),
                                 start_date=data['start_date'],
                                 end_date=data['end_date'],
                                 gender=(data['user_gender'] if 'user_gender' in data else None),
                                 category=(data['category'] if 'category' in data else None)).fetchall()
        output = CostOutputSerializer(listtodict(db_data, ['cost']), many=True)
        return Response(addrootelement(output.data))
