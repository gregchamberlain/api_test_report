from rest_framework import serializers


class BaseInputSerializer(serializers.Serializer):
    start_date = serializers.DateField()
    end_date = serializers.DateField()
    category = serializers.CharField(required=False)  # TODO make into Choice Field
    shipper = serializers.CharField(allow_null=True,required=False)
    groupby = serializers.ChoiceField(['month', 'week', 'daily'], required=False)
    scoretype = serializers.ChoiceField(['user','transaction','product'], required=False)


class ShipperPresenceInputSerializers(serializers.Serializer):
    shipper = serializers.CharField(required=True)

class LaneTopFiveInputSerializers(serializers.Serializer):
    pertype = serializers.ChoiceField(['total','ontime','delayed'], required=True)
    start_date = serializers.DateField()
    end_date = serializers.DateField()

class LanePerformanceInputSerializers(serializers.Serializer):
    destination = serializers.CharField(required=True)
    origin = serializers.CharField(required=True)

class PreventiveAlertsInputSerializers(serializers.Serializer):
    ptype= serializers.ChoiceField(['high','medium','low'])

class DistinctInputSerializer(BaseInputSerializer):
    limit = serializers.IntegerField(required=False)

class PreventiveAlertsDetailsInputSerializers(serializers.Serializer):
    orderlineid = serializers.IntegerField()

class BaseOutputSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    value = serializers.CharField()

class CostOutputSerializer(serializers.Serializer):
    cost = serializers.FloatField()

class ValueOutputSerializer(serializers.Serializer):
    value = serializers.FloatField()
