from django.conf import settings
from sqlalchemy import create_engine


def fetchandconverttodict(cursor, remap):
    "Return all rows from a cursor as a dict https://docs.djangoproject.com/en/1.9/topics/db/sql/#executing-custom-sql-directly"
    columns = [col[0] for col in cursor.description]
    newcolumns = list()
    for data in columns:
        if data == remap[0]:
            newcolumns.append(remap[1])
        else:
            newcolumns.append(data)
    print (columns)
    return [
        dict(zip(newcolumns, row))
        for row in cursor.fetchall()
        ]


def addrootelement(dictionary):
    '''
    This adds the root element to the return JSON output. The root element is data in this case, all other structures are contained in that.
    :param dictionary:
    :return: dictionary with root element as data
    '''
    data = dict()
    data['data'] = dictionary
    return data


def sqlengine():
    engine = create_engine(settings.SQL_ALCHEMY_DATABASE)
    return engine


def listtodict(listofdata, headers):
    data = list()
    if type(listofdata[0]) != dict:
        for items in listofdata:
            tmp = dict()
            for i in range(len(headers)):
                tmp[headers[i]] = items[i]
            data.append(tmp)
    else:
        return listofdata
    return data