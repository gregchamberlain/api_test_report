FROM ubuntu:16.04

RUN apt-get update && apt-get install -y \
	git \
	python \
	python-dev \
	python-setuptools \
	libpq-dev \
#	supervisor \
	python-pip  \
	python-psycopg2 && \
  	apt-get clean && \
  rm -rf /var/cache/apt/*

RUN pip install --upgrade pip

RUN pip install uwsgi

#COPY config/supervisord.conf /etc/supervisor/conf.d/

COPY requirements.txt /home/docker/veloapi/
RUN pip install -r /home/docker/veloapi/requirements.txt

COPY . /home/docker/veloapi/

WORKDIR /home/docker/veloapi/

EXPOSE 8903

#ENTRYPOINT ["supervisord", "-n"]
CMD ["uwsgi", "--http", ":8903", "--module", "testapi.wsgi"]
#CMD ["/bin/bash"]
